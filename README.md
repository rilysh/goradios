# Goradios

Go client for the [Radio Browser](https://api.radio-browser.info)

## Installation

````shell script
go get gitlab.com/AgentNemo/goradios
````

## Usage


````go
package main

import (
	"fmt"
	"gitlab.com/AgentNemo/goradios"
)

func main() {
	stations := goradios.FetchStations(goradios.StationsByCountry, "Germany")
	for _, station := range stations {
		fmt.Println(station)
	}
}
````

````go
package main

import (
	"fmt"
	"gitlab.com/AgentNemo/goradios"
)

func main() {
	countries := goradios.FetchCountriesDetailed(goradios.OrderName, false, false)
	for _, country := range countries {
		fmt.Println(country)
	}
}
````

## Development

- Download [Go](https://golang.org/dl/)
- Clone repo ````git clone git@gitlab.com:AgentNemo/goradios.git````

## Contributing

- Download [Go](https://golang.org/dl/)
- Fork repo
- Create a merge request

## TODOS

- Complete implementing all APIs
- Add tests
- Add ci/cd for tests
- 