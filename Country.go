package goradios

import (
	json2 "encoding/json"
	"strconv"
)

const (
	CountriesURL     = "https://de1.api.radio-browser.info/json/countries"
	CountriesCodeURL = "https://de1.api.radio-browser.info/json/countrycodes"
)

type Country struct {
	Name         string `json:"name"`
	StationCount int    `json:"stationcount"`
}

func FetchCountries() []Country {
	res := Post(StationsURL, "", nil)
	return UnmarshalCountries(res)
}

func FetchCountriesCode() []Country {
	res := Post(CountriesCodeURL, "", nil)
	return UnmarshalCountries(res)
}

func FetchCountriesDetailed(order Order, reverse bool, hideBroken bool) []Country {
	q := make(map[string]string)
	q["order"] = string(order)
	q["reverse"] = strconv.FormatBool(reverse)
	q["hidebroken"] = strconv.FormatBool(hideBroken)
	res := Post(CountriesURL, "", q)
	return UnmarshalCountries(res)
}

func FetchCountriesCodeDetailed(order Order, reverse bool, hideBroken bool) []Country {
	q := make(map[string]string)
	q["order"] = string(order)
	q["reverse"] = strconv.FormatBool(reverse)
	q["hidebroken"] = strconv.FormatBool(hideBroken)
	res := Post(CountriesCodeURL, "", q)
	return UnmarshalCountries(res)
}

func UnmarshalCountries(json string) []Country {
	var countries []Country
	json2.Unmarshal([]byte(json), &countries)
	return countries
}
