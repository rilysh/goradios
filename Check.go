package goradios

import (
	json2 "encoding/json"
)

const (
	ChecksURL = "https://de1.api.radio-browser.info/json/checks"
)

type Check struct {
	StationUUID               string `json:"stationuuid"`
	CheckUUID                 string `json:"checkuuid"`
	Source                    string `json:"source"`
	Codec                     string `json:"codec"`
	Bitrare                   int    `json:"bitrate"`
	HLS                       int    `json:"HLS"`
	OK                        int    `json:"ok"`
	Timestamp                 string `json:"timestamp"`
	URLCache                  string `json:"urlcache"`
	MetaInfoOverridesDatabase string `json:"metainfo_overrides_database"`
	Public                    int    `json:"public"`
	Name                      string `json:"name"`
	Description               string `json:"description"`
	Tags                      string `json:"tags"`
	CountryCode               string `json:"countrycode"`
	Homepage                  string `json:"homepage"`
	Favicon                   string `json:"favicon"`
	LoadBalancer              string `json:"loadbalancer"`
}

func FetchAllChecks() []Check {
	res := Post(ChecksURL, "", nil)
	return UnmarshalChecks(res)
}

func FetchAllChecksDetailed(stationuuid string, lastcheckuuid string, seconds uint) []Check {
	q := make(map[string]string)
	q["stationuuid"] = stationuuid
	q["lastcheckuuid"] = lastcheckuuid
	q["hidebroken"] = string(seconds)
	res := Post(ChecksURL, "", q)
	return UnmarshalChecks(res)
}

func FetchChecks(UUID string) []Check {
	res := Post(GenerateChecksURL(UUID), "", nil)
	return UnmarshalChecks(res)
}

func FetchChecksDetailed(UUID string, stationuuid string, lastcheckuuid string, seconds uint) []Check {
	q := make(map[string]string)
	q["stationuuid"] = stationuuid
	q["lastcheckuuid"] = lastcheckuuid
	q["hidebroken"] = string(seconds)
	res := Post(GenerateChecksURL(UUID), "", q)
	return UnmarshalChecks(res)
}

func UnmarshalChecks(json string) []Check {
	var checks []Check
	json2.Unmarshal([]byte(json), &checks)
	return checks
}

func GenerateChecksURL(UUID string) string {
	return ChecksURL + "/" + UUID
}
